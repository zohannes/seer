// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "STileGrid.generated.h"

class STile;

USTRUCT()
struct FGridRow 
{
	GENERATED_BODY()
public:

	TArray<STile*> Tiles;

	void Add(STile* Tile) 
	{
		Tiles.Add(Tile);
	}
};

UCLASS()
class SEER_API ASTileGrid : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASTileGrid();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	TArray<FGridRow> TileRows; // An array containing several row structs, creating a 2D Grid

	void GenerateGrid(int32 SizeX, int32 SizeY);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
