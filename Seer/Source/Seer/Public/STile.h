// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "STile.generated.h"

UCLASS()
class SEER_API ASTile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASTile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FVector2D GridPos; // Coordinates in X, Y

	bool IsOccupied;
	AActor* CurrentOccupant;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
