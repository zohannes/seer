// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SBaseGridActor.h"
#include "SCreature.generated.h"

UCLASS()
class SEER_API ASCreature : public ASBaseGridActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASCreature();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated)
	int32 Rarity;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated)
	int32 Dmg;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated)
	int32 Health;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated)
	int32 EnergyCost;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated)
	FString TxName;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated)
	FString TxFeat;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated)
	FString TxGroup;


	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
