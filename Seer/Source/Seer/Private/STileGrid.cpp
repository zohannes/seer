// Fill out your copyright notice in the Description page of Project Settings.

#include "STileGrid.h"
#include "STile.h"


// Sets default values
ASTileGrid::ASTileGrid()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASTileGrid::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASTileGrid::GenerateGrid(int32 SizeX, int32 SizeY)
{
	if (!SizeX|| !SizeY) return;

	for (int y = 0; y < SizeY; y++)
	{
		TileRows.Add(FGridRow());

		for (int x = 0; x < SizeX; x++)
		{
			//TileRows[y].Add();
		}
	}
}

// Called every frame
void ASTileGrid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

