// Fill out your copyright notice in the Description page of Project Settings.

#include "SCreature.h"
#include "UnrealNetwork.h"


// Sets default values
ASCreature::ASCreature()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ASCreature::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	DOREPLIFETIME(ASCreature, Rarity);

	DOREPLIFETIME(ASCreature, Dmg);
	DOREPLIFETIME(ASCreature, Health);
	DOREPLIFETIME(ASCreature, EnergyCost);

	DOREPLIFETIME(ASCreature, TxName);
	DOREPLIFETIME(ASCreature, TxFeat);
	DOREPLIFETIME(ASCreature, TxGroup);
}

// Called when the game starts or when spawned
void ASCreature::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASCreature::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

